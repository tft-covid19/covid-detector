import cv2
import numpy as np
import os
from threading import Thread, Lock
from .common.constants import IMAGE_SIZE
from .common.utils import logger as log
from sklearn.model_selection import train_test_split


class Dataset:

    """ This class represents the dataset itself """

    def __init__(self, dataset, file_load_mode=False):
        self.__info = dataset
        
        if(not file_load_mode):
            train, test = self.__get_samples(
                self.__info['path'],
                self.__info['classes'][0]
            )

            self.__train_images = train[0]
            self.__train_labels = train[1]
            self.__test_images = test[0]
            self.__test_labels = test[1]
        else:
            self.__train_images = None
            self.__train_labels = None
            self.__test_images = None
            self.__test_labels = None

    @property
    def info(self):
        return self.__info

    @property
    def train_images(self):
        return self.__train_images

    @property
    def train_labels(self):
        return self.__train_labels

    @property
    def test_images(self):
        return self.__test_images

    @property
    def test_labels(self):
        return self.__test_labels

    @info.setter
    def info(self, info):
        self.__info = info

    @train_images.setter
    def train_images(self, images):
        self.__train_images = images

    @train_labels.setter
    def train_labels(self, labels):
        self.__train_labels = labels

    @test_images.setter
    def test_images(self, images):
        self.__test_images = images

    @test_labels.setter
    def test_labels(self, labels):
        self.__test_labels = labels

    @staticmethod
    def __get_samples(main_directory_path, is_covid_class):
        """Returns the dataset divided in different areas"""

        images = []
        labels = []
        directories = os.listdir(main_directory_path)
        
        threads = []
        images_and_labels_lock = Lock()

        for directory in directories:
            log.info('Getting images from {}'.format(directory))
            
            thread = Thread(
                target=Dataset.__add_directory_images,
                args=(
                    main_directory_path,
                    directory,
                    images,
                    labels,
                    is_covid_class,
                    images_and_labels_lock
                )
            )

            threads.append(thread)
            thread.start()

        [thread.join() for thread in threads]

        log.info('Splitting dataset into training and test')
        train_images, test_images, train_labels, test_labels = train_test_split(
            images,
            labels
        )

        log.info('The dataset has been generated successfully')

        return (
            np.array(train_images),
            np.array(train_labels)
        ), (
            np.array(test_images),
            np.array(test_labels)
        )
    
    @staticmethod
    def __add_directory_images(
        root_path,
        directory,
        images,
        labels,
        is_covid_class,
        images_and_labels_lock
    ):

        samples = len(os.listdir(root_path + directory))

        for image in os.listdir(root_path + directory):
            if (samples == 0):
                break

            current_image = Dataset.__transform_image(os.path.join(
                root_path + directory + '/' + image))

            if(current_image is not None):
                images_and_labels_lock.acquire()
                images.append(current_image)
                Dataset.__classify_sample(labels, directory, is_covid_class, images_and_labels_lock)

            samples -= 1

    @staticmethod
    def __classify_sample(labels, sample, is_covid_class, images_and_labels_lock):

        labels.append(int(sample == is_covid_class))
        images_and_labels_lock.release()


    @staticmethod
    def __transform_image(image_path):

        """Resize the image and set its values between 0 and 1"""

        try:
            return cv2.resize(
                cv2.imread(image_path),
                (IMAGE_SIZE, IMAGE_SIZE),
                interpolation=cv2.INTER_AREA).astype('float32') / 255

        except cv2.error as Error:
            log.error(
                'There was an error with the image {}. It\'ll be skipped'.format(image_path))
            return None
