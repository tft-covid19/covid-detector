import tensorflow as tf
from ...common.constants import (
    PROJECT_BASE_PATH,
    DATASET_VARIABLE_DIRECTORY,
    RESOURCES_PATH
)
from ...common.utils.keras_callbacks import (
    STOP_IF_NOT_IMPROVING, 
    AUTO_REDUCE_LR_IF_NOT_IMPROVING, 
    STOP_ON_NAN_IN_LOSS
)

# DATASETS
DATASET_1_1 = {'path': '{}/data/dataset-1/Dataset All Augmented/Dataset All Augmented/'.format(PROJECT_BASE_PATH),
               'name': 'Dataset 1.1',
               'classes': ['COVID-19', 'Non-COVID-19']}
DATASET_1_2 = {'path': '{}/data/dataset-1/Dataset COVID-19 Augmented/Dataset COVID-19 Augmented/'.format(PROJECT_BASE_PATH),
               'name': 'Dataset 1.2',
               'classes': ['COVID-19', 'Non-COVID-19']}

# IMAGES
TRAIN_IMAGES_FILE_PATH = '{}/dataset_1/train_images'.format(DATASET_VARIABLE_DIRECTORY)
TRAIN_LABELS_FILE_PATH = '{}/dataset_1/train_labels'.format(DATASET_VARIABLE_DIRECTORY)
TEST_IMAGES_FILE_PATH = '{}/dataset_1/test_images'.format(DATASET_VARIABLE_DIRECTORY)
TEST_LABELS_FILE_PATH = '{}/dataset_1/test_labels'.format(DATASET_VARIABLE_DIRECTORY)

# MODEL PARAMETERS
CONV_WINDOW_SIZE = (5, 5)
POOLING_SIZE = (2, 2)
IMAGES_SHAPE = (200, 200, 3)
EPOCHS = 60
PADDING = 'same'
POOLING_PADDING = 'same'
POOLING_STRIDE = (2, 2)
BATCH_SIZE = 50
OPTIMIZER = 'Adam'
LOSS = tf.keras.losses.BinaryCrossentropy()
VALIDATION_SPLIT = 0.2
CONV_ACTIVATION = 'PReLU'
WEIGHT_INITIALIZER = tf.keras.initializers.TruncatedNormal()
CALLBACKS = [STOP_IF_NOT_IMPROVING,
             AUTO_REDUCE_LR_IF_NOT_IMPROVING,
             STOP_ON_NAN_IN_LOSS]

# OTHER
LOG_PATH = '{}/tomography/log/Dataset 1.1_weights_initializers_tests.csv'.format(RESOURCES_PATH)
