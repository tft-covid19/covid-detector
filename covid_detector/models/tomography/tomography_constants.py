import tensorflow as tf
from ...common.constants import (
    PROJECT_BASE_PATH,
    DATASET_VARIABLE_DIRECTORY,
    RESOURCES_PATH
)
from ...common.utils.keras_callbacks import (
    STOP_IF_NOT_IMPROVING, 
    AUTO_REDUCE_LR_IF_NOT_IMPROVING, 
    STOP_ON_NAN_IN_LOSS
)

# DATASETS
DATASET_2 = {'path': '{}/data/dataset-2/COVID-CT-master/Images-processed/'.format(PROJECT_BASE_PATH),
             'name': 'Dataset 2',
             'classes': ['CT_COVID', 'CT_NonCOVID']}

AUGMENTED_DATASET_2 = {
    'path': '{}/data/dataset-2/bright-contrast-augmented-dataset+/'.format(PROJECT_BASE_PATH),
    'name': 'Augmented dataset 2',
    'classes': ['CT_COVID', 'CT_NonCOVID']
}

# IMAGES
TRAIN_IMAGES_FILE_PATH = '{}/dataset_2/train_images'.format(DATASET_VARIABLE_DIRECTORY)
TRAIN_LABELS_FILE_PATH = '{}/dataset_2/train_labels'.format(DATASET_VARIABLE_DIRECTORY)
TEST_IMAGES_FILE_PATH = '{}/dataset_2/test_images'.format(DATASET_VARIABLE_DIRECTORY)
TEST_LABELS_FILE_PATH = '{}/dataset_2/test_labels'.format(DATASET_VARIABLE_DIRECTORY)

# MODEL PARAMETERS
CONV_WINDOW_SIZE = (5, 5)
POOLING_SIZE = (2, 2)
IMAGES_SHAPE = (200, 200, 3)
EPOCHS = 60
PADDING = 'same'
POOLING_PADDING = 'same'
POOLING_STRIDE = (2, 2)
BATCH_SIZE = 50
OPTIMIZER = 'Adam'
LOSS = tf.keras.losses.BinaryCrossentropy()
VALIDATION_SPLIT = 0.2
CONV_ACTIVATION = 'relu'
WEIGHT_INITIALIZER = tf.keras.initializers.GlorotNormal()
CALLBACKS = [STOP_IF_NOT_IMPROVING,
             AUTO_REDUCE_LR_IF_NOT_IMPROVING,
             STOP_ON_NAN_IN_LOSS]

# OTHER
LOG_PATH = '{}/x-ray/log/Dataset_2_tests.csv'.format(RESOURCES_PATH)
