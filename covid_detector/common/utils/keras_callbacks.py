import tensorflow as tf

STOP_IF_NOT_IMPROVING = tf.keras.callbacks.EarlyStopping(
    patience=10,
    restore_best_weights=False
)

AUTO_REDUCE_LR_IF_NOT_IMPROVING = tf.keras.callbacks.ReduceLROnPlateau(
    patience=10
)

STOP_ON_NAN_IN_LOSS = tf.keras.callbacks.TerminateOnNaN()
