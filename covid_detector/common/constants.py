# IMAGE
IMAGE_SIZE = 200

# PATHS
PROJECT_BASE_PATH = '/content/gdrive/MyDrive/ULPGC/TFT'
REPO_BASE_URL = '{}/projects/COVID-detector'.format(PROJECT_BASE_PATH)
DATASET_VARIABLE_DIRECTORY = '/content/gdrive/MyDrive/ULPGC/TFT/data/data_variable_files'
RESOURCES_PATH = '{}/resources'.format(REPO_BASE_URL)